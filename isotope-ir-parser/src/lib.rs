/*!
A parser for the `isotope` intermediate representation
*/
#![forbid(unsafe_code, missing_debug_implementations, missing_docs)]
pub use elysees::Arc;

pub mod ast;
pub mod parser;